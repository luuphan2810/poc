"""
    Created by nguyenvanhieu.vn at 9/16/2018
"""
from flask import Flask, render_template, redirect, url_for, request
import psycopg2
import pandas as pd
import cx_Oracle
from sqlalchemy import create_engine
import datetime
import numpy as np
 
app = Flask(__name__)
def get_panda_data():
    tags = ["<h1>Example header</h1>",
            '<div style="color: red;">Example div</div>',
            '<input type="text" name="example_form" placeholder="Example input form">']
    pd.set_option('display.max_colwidth', -1)
    tags_frame = pd.DataFrame(tags, columns = ["Tag Example"])
    tags_html = tags_frame.to_html(escape=False)
    return tags_html    
 
@app.route('/')
def welcome():
    return redirect('/login')
 
 
@app.route('/home')
def home():
    return 'Login success!'
 
 
# Route for handling the login page logic
@app.route('/login', methods=['GET', 'POST'])
def login():
    error = None
    if request.method == 'POST':
        if request.form['username'] != 'admin' or request.form['password'] != 'admin':
            error = 'Invalid Credentials. Please try again.'
        else:
            return redirect(url_for('calculate'))
    return render_template('login.html', error=error)

# Route for handling the calculate page logic
@app.route('/calculate', methods=['GET', 'POST'])
def calculate():
    error = None
    if request.method == 'POST':    
        #Connection string
        conPostgres = psycopg2.connect(host="localhost",database="postgres", user="postgres", password="12345678x@X")
        sqlPostgres = "SELECT mathuoc, tenthuoc FROM THUOC"

        #Load data to data frame and modify
        dfPostgres = pd.read_sql(sqlPostgres, conPostgres)
        currentDT = datetime.datetime.now()
        fileName = 'd:\\'+str(currentDT).lstrip().replace(" ", "_").replace(":", "_")+'.csv'
        dfPostgres.to_csv(fileName, encoding='utf-8')
        #dfPostgres.to_html('analysis.html')
        
        #Close connection
        #conPostgres.close()
        html_data = get_panda_data()
        return render_template("analysis.html", data=dfPostgres.to_html())

    return render_template('calculate.html', error=error)
 
if __name__ == '__main__':
    app.run(host='localhost', port=5000, debug=True)